from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptForm, ExpebseCategoryForm, AccountForm
from django.db.models import Count


@login_required
def list_receipt(request):
    lists = Receipt.objects.filter(purchaser=request.user)
    context = {"list_objects": lists}
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            item = form.save(False)
            item.purchaser = request.user
            item.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {"form": form}
    return render(request, "receipts/create_receipt.html", context)


@login_required
def show_category(request):
    categories = ExpenseCategory.objects.filter(owner=request.user).annotate(
        num_expenses=Count("receipts")
    )
    context = {"category_objects": categories}
    return render(request, "receipts/category.html", context)


@login_required
def show_account(request):
    accounts = Account.objects.filter(owner=request.user).annotate(
        num_of_receipts=Count("receipts")
    )
    context = {"account_objects": accounts}
    return render(request, "receipts/account.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpebseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = ExpebseCategoryForm()
    context = {"form": form}
    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {"form": form}
    return render(request, "receipts/create_account.html", context)
