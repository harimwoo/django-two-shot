from django.urls import path
from receipts.views import (
    list_receipt,
    create_receipt,
    show_category,
    show_account,
    create_category,
    create_account,
)

urlpatterns = [
    path("", list_receipt, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", show_category, name="category_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/", show_account, name="account_list"),
    path("accounts/create/", create_account, name="create_account"),
]
